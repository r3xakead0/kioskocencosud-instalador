﻿using System;
using System.IO;
using System.Diagnostics;
using System.Configuration;
using System.Net;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using Newtonsoft.Json;
using Ionic.Zip;
using System.Reflection;
using System.Text;
using FluentFTP;
using Progression;
using System.Threading;
using RestSharp;

namespace Cupones.Installer
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    class Program
    {

        #region Configuraciones

        private static string ambito = "";

        private static string ftpHost = "";
        private static string ftpUser = "";
        private static string ftpPass = "";

        private static string credentialUrl = "";
        private static string vridgeUrl = "";

        #endregion

        private static string pathExe = "";
        private static string urlFtp = "";

        private static string pathRoot = "";
        private static string pathUpdate = "";
        private static string pathTemp = "";

        static void Main(string[] args)
        {
            try
            {
                #region Cargar configuraciones

                if (args.Length > 0)
                {
                    UpdateSetting("ambito", args[0].ToString());
                }

                ambito = ConfigurationManager.AppSettings["ambito"];

                ftpHost = ConfigurationManager.AppSettings["ftpHost"];
                ftpUser = ConfigurationManager.AppSettings["ftpUser"];
                ftpPass = ConfigurationManager.AppSettings["ftpPass"];

                credentialUrl = ConfigurationManager.AppSettings["credentialUrl"];
                vridgeUrl = ConfigurationManager.AppSettings["vridgeUrl"];

                #endregion

                pathExe = @"C:\" +  ambito + @"\Cupones.exe";
                pathRoot = Path.GetDirectoryName(pathExe);
                pathUpdate = Path.Combine(pathRoot, "update");
                pathTemp = Path.Combine(pathRoot, "temp");

                string lastVersion = "";
                string title = "";

                #region Version de Instalador

                title = "INSTALADOR";
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(CentrarTexto(title, Console.WindowWidth));
                Console.ResetColor();

                string instaladorVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                Console.WriteLine("Version\t : " + instaladorVersion);
                Console.WriteLine();

                #endregion

                #region Validar Conexion a Red

                title = "VALIDAR LA CONEXION";
                Console.BackgroundColor = ConsoleColor.DarkCyan;
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(IzquierdaTexto(title, Console.WindowWidth));
                Console.ResetColor();

                bool isConnected;

                Console.Write("Esperando conexion de RED");
                isConnected = false;
                while (isConnected == false)
                {
                    isConnected = CheckNetworkAvailable();
                }
                Console.WriteLine("\rConexion de RED establecida");

                Console.Write("Esperando conexion al FTP");
                isConnected = false;
                while (isConnected == false)
                {
                    isConnected = CheckFtpAvailable();
                }
                Console.WriteLine("\rConexion al FTP establecida");

                Console.Write("Esperando conexion al VRIDGE");
                isConnected = false;
                while (isConnected == false)
                {
                    isConnected = CheckForInternetConnection(credentialUrl, vridgeUrl);
                }
                Console.WriteLine("\rConexion al VRIDGE establecida");

                Console.WriteLine();

                #endregion

                #region Obtener Version de Software

                title = "VALIDAR VERSION DEL SOFTWARE";
                Console.BackgroundColor = ConsoleColor.DarkCyan;
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(IzquierdaTexto(title, Console.WindowWidth));
                Console.ResetColor();

                Console.WriteLine(pathExe);

                #region Obtener Version Local

                string versionLocal = GetLocalVersion(pathExe);

                Console.WriteLine("Version Local\t : " + versionLocal);

                #endregion

                #region Obtener Version Remota

                urlFtp = @"/Deploy/" + ambito + @"/";

                string fileRemota = "";
                string checksumRemota = "";

                string versionRemota = GetRemoteVersion("update.json", out fileRemota, out checksumRemota);

                Console.WriteLine("Version Remota\t : " + versionRemota);

                #endregion

                Console.WriteLine();

                #endregion

                #region Descarga Version Remota

                lastVersion = versionLocal;
                if (versionLocal.Equals(versionRemota) == false)
                {

                    lastVersion = versionRemota;

                    title = "DESCARGANDO LA VERSION " + versionRemota + " DEL SOFTWARE";

                    Console.BackgroundColor = ConsoleColor.DarkCyan;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(IzquierdaTexto(title, Console.WindowWidth));
                    Console.ResetColor();

                    string pathZipFile = Path.Combine(pathUpdate, fileRemota);
                    string checksumLocal = "";
                    if (DownloadVersion(fileRemota) == true)
                    {
                        checksumLocal = GetChecksum(pathZipFile);
                    }
                    Console.WriteLine();

                    Console.WriteLine("Checksum MD5 Local\t : " + checksumRemota);
                    Console.WriteLine("Checksum MD5 Remota\t : " + checksumLocal);

                    Console.WriteLine();

                    if (checksumRemota.Equals(checksumLocal))
                    {
                        Console.WriteLine("Actualizando a la version " + versionRemota);
                        bool update = UpdateVersion(pathZipFile);

                        if (update)
                            Console.WriteLine("Actualizacion Completada");
                        else
                            throw new Exception("Actualizacion Incompleta");
                    }
                    else
                    {
                        throw new Exception("Error al descargar la nueva version");
                    }

                    Console.WriteLine();

                }

                #endregion

                #region Ejecutar Aplicativo

                title = "EJECUTANDO VERSION " + lastVersion;
                Console.BackgroundColor = ConsoleColor.DarkCyan;
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(IzquierdaTexto(title, Console.WindowWidth));
                Console.ResetColor();

                Process.Start(pathExe);
                //Console.Read();
                #endregion


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                Thread.Sleep(10000); //Reiniciar despues de 10 segundos
                Process.Start(AppDomain.CurrentDomain.FriendlyName);
                Environment.Exit(0);
            }

        }

        public static string CentrarTexto(string texto, int longitud)
        {
            int spaces = longitud - texto.Length;
            int padLeft = spaces / 2 + texto.Length;

            return texto.PadLeft(padLeft).PadRight(longitud);
        }

        public static string IzquierdaTexto(string texto, int longitud)
        {
            return texto.PadRight(longitud);
        }

        private static void UpdateSetting(string key, string value)
        {
            Configuration configuration = ConfigurationManager.
                OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);
            configuration.AppSettings.Settings[key].Value = value;
            configuration.Save();

            ConfigurationManager.RefreshSection("appSettings");
        }

        private static bool DownloadVersion(string zipFileName)
        {
            bool flag = false;
            try
            {

                string urlZipFile = Path.Combine(urlFtp, zipFileName).Replace("\\", "/");
                string pathZipFile = Path.Combine(pathUpdate, zipFileName);

                if (Directory.Exists(pathUpdate) == true)
                    Directory.Delete(pathUpdate, true);

                Directory.CreateDirectory(pathUpdate);

                if (File.Exists(pathZipFile) == true)
                    File.Delete(pathZipFile);


                //Conectarse al FTP
                using (var ftp = new FtpClient())
                {
                    ftp.Encoding = Encoding.GetEncoding(1252);
                    ftp.Host = ftpHost;
                    ftp.Credentials = new NetworkCredential(ftpUser, ftpPass);

                    //Descargar archivo via stream desde el FTP
                    using (var remoteFileStream = ftp.OpenRead(urlZipFile, FtpDataType.Binary))
                    {
                        using (var newFileStream = File.Create(pathZipFile))
                        {
                            byte[] buffer = new byte[8 * 1024];

                            //Configurando la barra de progreso
                            int total = (int)remoteFileStream.Length;
                            var progressBar = new ProgressBar(total, "Descargando", Console.WindowWidth - 20);

                            //Descargando el archivo localmente via stream
                            int totalReadBytesCount = 0;
                            int readBytesCount;
                            while ((readBytesCount = remoteFileStream.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                newFileStream.Write(buffer, 0, readBytesCount);
                                totalReadBytesCount += readBytesCount;

                                //Actualizar progreso de la barra
                                progressBar.UpdateStatus(totalReadBytesCount);
                            }
                        }
                    }

                    // read the FTP response and prevent stale data on the socket
                    ftp.GetReply();
                    
                }


                if (File.Exists(pathZipFile) == true)
                {
                    if (Directory.Exists(pathTemp) == true)
                        Directory.Delete(pathTemp, true);

                    Directory.CreateDirectory(pathTemp);

                    using (ZipFile zipFile = new ZipFile(pathZipFile))
                    {
                        zipFile.ExtractAll(pathTemp, ExtractExistingFileAction.OverwriteSilently);
                    }

                    string[] filesTemp = Directory.GetFiles(pathTemp);
                    foreach (var file in filesTemp)
                    {
                        var fileName = Path.GetFileName(file);
                        var destFile = Path.Combine(pathRoot, fileName);
                        File.Copy(file, destFile, true);
                    }

                    Directory.Delete(pathTemp, true);

                    flag = true;

                }

                return flag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Actualizar la version de software
        /// </summary>
        /// <param name="pathZipFile">Ruta del archivo zip</param>
        /// <param name="pathRoot">Ruta de la ruta destino</param>
        /// <returns></returns>
        private static bool UpdateVersion(string pathZipFile)
        {
            bool flag = false;
            try
            {

                if (Directory.Exists(pathTemp) == true)
                    Directory.Delete(pathTemp, true);

                Directory.CreateDirectory(pathTemp);
             
                if (File.Exists(pathZipFile) == true)
                {

                    using (ZipFile zipFile = new ZipFile(pathZipFile))
                    {
                        zipFile.ExtractAll(pathTemp, ExtractExistingFileAction.OverwriteSilently);
                    }

                    string[] filesTemp = Directory.GetFiles(pathTemp);
                    foreach (var file in filesTemp)
                    {
                        var fileName = Path.GetFileName(file);
                        var destFile = Path.Combine(pathRoot, fileName);
                        File.Copy(file, destFile, true);
                    }

                    Directory.Delete(pathTemp, true);

                    flag = true;

                }

                return flag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static string GetLocalVersion(string pathExe)
        {
            string version = "0.0.0.0";
            try
            {
                if (File.Exists(pathExe))
                {
                    var versionInfo = FileVersionInfo.GetVersionInfo(pathExe);
                    if (versionInfo != null)
                        version = versionInfo.ProductVersion;
                }

                return version;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static string GetRemoteVersion(string fileJson, out string file, out string checksum)
        {
            string versionRemota = "0.0.0.0";
            string fileRemota = "";
            string checksumRemota = "";
            try
            {

                string urlJson = Path.Combine(urlFtp, fileJson).Replace("\\", "/");
                string pathJson = Path.Combine(pathUpdate, Path.GetFileName(urlJson));

                if (Directory.Exists(pathUpdate) == true)
                    Directory.Delete(pathUpdate, true);

                Directory.CreateDirectory(pathUpdate);

                if (File.Exists(pathJson) == true)
                    File.Delete(pathJson);

                //Conectarse al FTP
                using (var ftp = new FtpClient())
                {
                    ftp.Encoding = Encoding.GetEncoding(1252);
                    ftp.Host = ftpHost;
                    ftp.Credentials = new NetworkCredential(ftpUser, ftpPass);

                    //Reintentar 3 veces la descarga del archivo 
                    ftp.RetryAttempts = 3;

                    //Descargar del archivo con reintentos
                    ftp.DownloadFile(pathJson, urlJson, true, FtpVerify.Retry);
                }

                if (File.Exists(pathJson) == true)
                {
                    string json = File.ReadAllText(pathJson);

                    var definition = new { version = "", file = "", checksum = "", };
                    var config = JsonConvert.DeserializeAnonymousType(json, definition);

                    versionRemota = config.version;
                    fileRemota = config.file;
                    checksumRemota = config.checksum;
                }

                file = fileRemota;
                checksum = checksumRemota;

                return versionRemota;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static string GetChecksum(string filename)
        {
            try
            {
                using (var md5 = MD5.Create())
                {
                    using (var stream = File.OpenRead(filename))
                    {
                        var hash = md5.ComputeHash(stream);
                        return BitConverter.ToString(hash).Replace("-", "").ToUpper();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool CheckNetworkAvailable()
        {
            return NetworkInterface.GetIsNetworkAvailable();
        }

        public static bool CheckFtpAvailable()
        {
            bool isAvailable = false;
            try
            {
                using (var ftp = new FtpClient())
                {
                    ftp.Host = ftpHost;
                    ftp.Credentials = new NetworkCredential(ftpUser, ftpPass);

                    ftp.Connect();

                    isAvailable = ftp.IsConnected;
                }

               
            }
            catch (Exception ex)
            {
                isAvailable = false;
            }

            return isAvailable;
        
        }

        private static void ClearFolder(string FolderName)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(FolderName);

                foreach (FileInfo fi in dir.GetFiles())
                {
                    fi.Delete();
                }

                foreach (DirectoryInfo di in dir.GetDirectories())
                {
                    ClearFolder(di.FullName);
                    di.Delete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Validar si existe conexión y disponibilidad con el WS de Vridge
        /// </summary>
        /// <param name="credentialUrl">Url de WS para generar TOKEN</param>
        /// <param name="vridgeUrl">Url de WS para realizar consulta</param>
        /// <returns></returns>
        public static bool CheckForInternetConnection(string credentialUrl, string vridgeUrl)
        {
            bool result = false;
            try
            {
                RestClient restClient = null;
                RestRequest restRequest = null;
                string json = "";

                #region Obtener token de seguridad
                Uri baseCredentialUri = new Uri(credentialUrl);
                Uri credentialUri = new Uri(baseCredentialUri, "/oauth2/token");

                restClient = new RestClient(credentialUri);

                restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("content-type", "application/x-www-form-urlencoded");

                string credentials = "client_credentials";
                string id = "QUIGYWTGGYSHIEI4QB8M";
                string secret = "ECF7M5XXXQRFU6M0R1F3";
                restRequest.AddParameter("application/x-www-form-urlencoded",
                                            string.Format("grant_type={0}&client_id={1}&client_secret={2}&",
                                            (object)credentials, (object)id, (object)secret), ParameterType.RequestBody);

                json = restClient.Execute(restRequest).Content;
                #endregion

                if (json.Length > 0)
                {
                    string token = ""; //Valor del token de seguridad

                    var tokenDef = new { access_token = "", token_type = ""};
                    var tokenObj = JsonConvert.DeserializeAnonymousType(json, tokenDef);
                    if (tokenObj != null)
                        token = tokenObj.token_type + " " + tokenObj.access_token;

                    #region Obtener lista de companias 
                    Uri baseVridgeUri = new Uri(vridgeUrl);
                    Uri vridgeUri = new Uri(baseVridgeUri, "/?r=site/mostrar-companias-v2");

                    restClient = new RestClient(vridgeUri);

                    restRequest = new RestRequest(Method.POST);
                    restRequest.AddHeader("cache-control", "no-cache");
                    restRequest.AddHeader("authorization", token);

                    json = restClient.Execute(restRequest).Content;
                    #endregion 

                    if (json.Length > 0)
                        result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
    }

}
